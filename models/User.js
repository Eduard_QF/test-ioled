'use strict';
const bycrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @description esquema base para el ususario obtenido desde el logueo.
 * @param providerId id entregado por el proveedor de los datos.
 * @param name nombre del usuario.
 * @param email correo del usuario.
 * @param photo fotografia del usuario.
 * @param provider proveedor con el que se registro el ususario.
 * @constant timestamps datos creado por la base de datos de la fecha de creacion del registro.  
 */
let userSchema = new Schema({
    providerId: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    photo: {
        type: String,
        require: true
    },
    provider: {
        type: String,
        require: true
    },
}, {
    timestamps: true
});

/**
 * @description funcion que encrpta la contraseña para un mayor efecto de seguridad.
 * @param save momento de activacion de la funcion.
 * @callback funtion callback que encripta la clave.
 */
userSchema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('password')) {
        return next();
    }

    bycrypt.genSalt(10, (err, salt) => {
        if (err) {
            next(err);
        }
        bycrypt.hash(user.password, salt, null, (err, hash) => {
            if (err) {
                next(err);
            }
            user.password = hash;
            next();
        });
    });
});

/**
 * @description funcion dedicada a comparar la clave entregada por el usuario al momento del login, y compararla con el encriptada en la base de datos.
 * @param password contraseña entrgada por el usuario.
 * @param cb callback de compracion.
 */
userSchema.method.comparePassword = function (password, cb) {
    bycrypt.compare(password, this.password, (err, equals) => {
        if (err) {
            return cb(err);
        }
        cb(null, equals);
    });
};

module.exports = mongoose.model('user', userSchema);