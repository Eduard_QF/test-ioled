'use strict'
const router = require('express').Router();
const passport = require('passport');
const User = require('../models/User');
const userController = require('../controllers/userController');
/**
 * @description funcion encargada de validar que el usuario se encuentra logueado.
 * @param {*} req request del navegador.
 * @param {*} res respuesta al servidor.
 */
const authCheck = (req, res, next) => {
    if (!req.user) {
        res.json({ 'response': 'usuario no logeado' });
    } else {
        next();
    }
}
/**
 * @description ruta para el proceso de auntenticacion.
 * @param {string} auth_google ruta para el login con google.
 * @param passport.authenticate midelware encargado de la autenticacion de google
 */
router.get('/auth/google', passport.authenticate('google', {
    scope: ['profile']
}));

/**
 * @description ruta para el proceso creacion de cuenta sin redes sociales.
 * @param {string} auth_signup ruta para el login sin redes sociales.
 * @param userController.postSignup funcion encargado del registro sin redes sociales.
  
*/
router.post('/auth/signup', userController.postSignup);

/**
 * @description ruta para el proceso de auntenticacion sin redes sociales. 
 * @param {string} auth_login ruta para el login sin redes sociales.
 * @param userController.postLogin funcion encargado del ingreso sin redes sociales.
*/
router.post('/auth/login', userController.postLogin);

/**
 * @description ruta para el usuario actual.
 * @param {string} auth_google_redirect ruta para el login con google.
 * @param passport.authenticate midelware encargado de la autenticacion de google.
 * @callback callback funcion de respuesta de la peticion, reenvia a la ruta /auth/.
*/
router.get('/auth/google/redirect', passport.authenticate('google'), (req, res) => {
    console.log(req.user);
    res.redirect('/auth/');
});

/**
 * @description ruta para ver el usuario actual.
 * @param {string} auth ruta de la peticion.
 * @param authCheck funcion que valida q el usuario este logueado.
 * @callback callback muetra datos del usuario actual.
*/
router.get('/auth/', authCheck, (req, res) => {
    req.session.cuenta = req.session.cuenta ? req.session.cuenta + 1 : 1;
    res.json({
        'usuario': {'name':req.user.name,'provider':req.user.provider,'photo':req.user.photo},
        'ingreso del navegador': req.session.cuenta
    });
});

/**
 * @description ruta para finalizar la session
 * @param {string} auth_logout ruta de la peticion para finalizar la sesión.
 * @param authCheck funcion que valida q el usuario este logueado.
 * @callback callback cierra la sesion actual.
*/
router.get('/auth/logout', authCheck, userController.logout);

module.exports = router;