'use strict'
/**
 * @api microservicio para el registro de ususarios.
 * @description microservicio para el ingreso de usuarios a traves de oauth2.0 y google.
 * @version 1.0.0
 */

const express = require('express');
const passport = require('passport');
const session = require('express-session');
const bodyParser = require ('body-parser');
const MongoStore = require('connect-mongo')(session);
const authRouthes = require('./routes/auth-routes')
const passportSetup = require('./config/passport');
const mongooseSetup = require('./config/mongoose');
const keys = require('./config/keys');

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use(session({
    secret:keys.session.secret,
    resave:true,
    saveUninitialized:true,
    store: new MongoStore({
        url:keys.mongodb.dbURI,
        autoReconnect:true
    })
}))

app.use(passport.initialize());
app.use(passport.session());

app.use(authRouthes);
app.listen(keys.server.port,()=>{
    console.log(`Server ejecutandose en el puerto ${keys.server.port}`);
});

app.get('/',(req,res)=>{
    res.json('url funcionando');
})

module.exports={
    app
};

