'use strict'

const passaport = require ('passport');
const User = require('../models/User');

/**
 * @description funcion para el registro de un nuevo usuario.
 * @param req request del navegador.
 * @param res respuesta al navegador.
 */
exports.postSignup =(req,res,next)=>{
    const newUser = new User({
        email:req.body.email,
        nombre: req.body.name,
        password: req.body.password
    });

    User.findOne({email:req.body.email},(err,userExist)=>{
       if(userExist){
           return res.status(400).send('Email register in the database');
       } 
       newUser.save((err)=>{
           if(err){
               next(err);
           }
           req.logIn(newUser,(err)=>{
               if(err){
                   next(err);
               }
               res.send('User created successful');
           });
       });
    });
}

/**
 * @description funcion para el ingreso de un usuario ya registrado.
 * @param req request del navegador.
 * @param res respuesta al navegador.
 */
exports.postLogin=(req,resp,next)=>{
    passaport.authenticate('local',(err,user,info)=>{
        if(err){
            next(err);
        }
        if(!user){
            return res.status(400).send('Email or password is not valid')
        }
        req.logIn(user,(err)=>{
            if(err){
                next(err);
            }
            res.send('Login successful');
        });
    });
}

/**
 * @description funcion para la salida de un ususario.
 * @param req request del navegador.
 * @param res respuesta al navegador.
 */
exports.logout= (req,res)=>{
    req.logout();
    res.json('Logout successful');
}