'use strict'
const passport = require('passport');
//const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const GoogleStrategy = require('passport-google-oauth20');
const keys = require('./keys');

/**
 * @description insercion del midelware encargado de la autenticacion con google-auth2.0 con la estrategia de google
 */
passport.use(
    new GoogleStrategy(
        {
            callbackURL: '/auth/google/redirect',
            clientID: keys.google.clientId,
            clientSecret: keys.google.clientSecret
        }, (accessToken, refreshToken, profile, done) => {
            User.findOne({ providerId: profile.id })
                .then((currentUser) => {
                    if (currentUser) {
                        console.log(`current user: ${currentUser.providerId}`);
                        try{
                            done(null,currentUser);    
                        }catch(err){
                            console.log(err);
                        }
                        
                    }
                    else {
                        new User({
                            name: profile.displayName,
                            providerId: profile.id,
                            photo: profile.photos[0].value,
                            provider: profile.provider
                        }).save().then((newUser) => {
                            console.log(`new user: ${newUser.providerId}`);
                            done(null,newUser);
                        })
                    }
                    
                }).catch()
        }
    )
    /*,new LocalStrategy(
        {
            usernameField: 'email'
        }, (email, passport, done) => {
            User.findOne({ email }, (err, user) => {
                if (!user) {
                    return done(null, false, { message: `email ${email} not register in database` });
                } else {
                    return done(null, user);
                }
            })
        }
    )*/
);

/**
 * @description serializacion de los datos de los usuarios a traves del midelware provisto por passaport.
 */
passport.serializeUser((user, done) => {
    done(null, user._id);

});


/**
 * @description deserializacion de los datos de los usuarios a traves del midelware provisto por passaport.
 */
passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});