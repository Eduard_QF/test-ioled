'use strict';
const mongoose = require('mongoose');
const keys = require('./keys');

/**
 * @description Conección a la base de datos
 * @param {string} keys.mongodb.dbURI direccion de la base de datos.
 * @callback callback  callback de la llamada a la conneción de la base de datos.
 */
mongoose.connect(keys.mongodb.dbURI,(err,res)=>{
    if (err) throw err;

    console.log('Base de datos conectada');
});


module.exports = {
    mongoose
};