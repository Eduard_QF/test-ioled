/**
 * @description claves para la configuración del micro servicio.
 * @var google configuraciones de google.
 * @var google.clientId id del cliente de la api de google para el uso de oauth20
 * @var google.clientSecret secreto del cliente de la api de google para el uso de oauth20
 * @var mongodb configuración de la base de datos.
 * @var mongodb.dbURI dirección url de la localización de la base de datos.
 * @var server configuración del servidor alojado.
 * @var server.port puerto por omisión en caso de que el servidor no entregue uno.
 * @var session configuración de express-session para el uso de cookes.
 * @var session.secret secreto para realizar el hash de las cookes.
 * @version 1.0.0
 */
module.exports ={
    google:{
        clientId:'1094568749380-vs6vg74s49rrsoevp47sg58n29j7alp9.apps.googleusercontent.com',
        clientSecret:'4OPNJb8_y_4b0cOG-UM3jzY6'
    },
    mongodb:{
        dbURI:'mongodb+srv://mongotest:jaAta5FsEehA5xM@cluster0-ifuvi.gcp.mongodb.net/test?retryWrites=true&w=majority'
    },
    server:{
        port:8080
    },
    session:{
        secret:'secret'
    }
}